package com.antoonvereecken;

import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HelloWorld extends AbstractBehavior<HelloWorld.Command> {    // <T> what kind of msg can the actor receive
    /** best practice: first, nested marker interface */
    interface Command { }

    /** next, define the concrete messages the actor will accept, all msg = immutable */
    // triggers actor to say hello - since it has no params, we can define it as a singleton (using enum with a single value)
    public enum SayHello implements Command {
        INSTANCE
    }
    // to change the msg we need param & immutability -> static class, no getters/setters, public final fields
    public static class ChangeMessage implements Command {
        public final String newMessage;

        public ChangeMessage(String newMessage) {
            this.newMessage = newMessage;
        }
    }

    /** factory method: creates a context for the behavior */
    public static Behavior<HelloWorld.Command> create() {
        return Behaviors.setup(context -> new HelloWorld(context));
    }

    /** since we can change the message, we need to keep this as a mutable field inside the actor
     *      -> we initialize it so can "sayHello" without first changing the message
     */
    private String message = "HelloWorld";

    /** default constructor, private because we have a public factory method: ".create()"  */
    private HelloWorld(ActorContext<HelloWorld.Command> context) {
        super(context);
    }

    /** the actual logic that is triggered when a message is received */
    @Override
    public Receive<Command> createReceive() {
        return newReceiveBuilder()
                .onMessageEquals(SayHello.INSTANCE, this::onSayHello)
                .onMessage(ChangeMessage.class, this::onChangeMessage)
                .build();
    }

    /** handlers/actions */
    private Behavior<Command> onChangeMessage(ChangeMessage changeMessageCommand) {     // changes the message
        message = changeMessageCommand.newMessage;
        return this;    // return current behavior (= instance of HelloWorld.class)
    }
    private Behavior<Command> onSayHello() {                                            // log message
        log.info(message);
        return this;    // return current behavior
    }

}