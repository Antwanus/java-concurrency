package com.antoonvereecken;

import akka.actor.typed.ActorSystem;

public class HelloWorldApp {
    public static void main(String[] args) {
        ActorSystem<HelloWorld.Command> mySystem = ActorSystem.create(HelloWorld.create(), "MySystem");

        mySystem.tell(HelloWorld.SayHello.INSTANCE);
        mySystem.tell(HelloWorld.SayHello.INSTANCE);
        mySystem.tell(HelloWorld.SayHello.INSTANCE);

        mySystem.tell(new HelloWorld.ChangeMessage("Hello Mars!!"));
        mySystem.tell(HelloWorld.SayHello.INSTANCE);
        mySystem.tell(HelloWorld.SayHello.INSTANCE);
    }


}
