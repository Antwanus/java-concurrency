package com.antoonvereecken.behavior;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;

import java.math.BigInteger;
import java.util.Random;
import java.util.UUID;

public class MyWorkerBehavior extends AbstractBehavior<MyWorkerBehavior.Command> {
    /** nester marker interface */
    public interface Command { }

    /** msg */
    public enum CalculatePrime implements Command { INSTANCE }
    public enum SpawnActor implements Command { INSTANCE }

    /** default (private) constructor + (public static) factory method */
    private MyWorkerBehavior(ActorContext<Command> context) {
        super(context);
    }
    public static Behavior<Command> create() {
        return Behaviors.setup(MyWorkerBehavior::new);
    }

    /** action */
    @Override
    public Receive<Command> createReceive() {
        return newReceiveBuilder()
                .onMessageEquals(CalculatePrime.INSTANCE, this::onCalculatePrime)
                .onMessageEquals(SpawnActor.INSTANCE, this::onSpawnActor)
                .build();
    }
    /** handler */
    private Behavior<MyWorkerBehavior.Command> onSpawnActor() {
        ActorRef<Command> a = getContext().spawn(MyWorkerBehavior.create(), UUID.randomUUID().toString());
        a.tell(CalculatePrime.INSTANCE);
        return this;
    }
    private Behavior<MyWorkerBehavior.Command> onCalculatePrime() {
        BigInteger bigInteger = new BigInteger(2000, new Random());
        System.out.println(bigInteger.nextProbablePrime());
        return this;
    }
}
