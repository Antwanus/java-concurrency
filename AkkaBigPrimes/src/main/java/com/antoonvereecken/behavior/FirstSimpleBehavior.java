package com.antoonvereecken.behavior;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;

public class FirstSimpleBehavior extends AbstractBehavior<String> {

    private FirstSimpleBehavior(ActorContext<String> context) {
        super(context);
    }
    public static Behavior<String> create() {
        return Behaviors.setup(FirstSimpleBehavior::new);
    }

    // how an actor responds to a message
    @Override
    public Receive<String> createReceive() {
        return newReceiveBuilder()
                .onMessageEquals("say hi", () -> {
                    System.out.println("hi");
                    return this;
                })
                .onMessageEquals("who are u", () -> {
                    System.out.println("My path is " + getContext().getSelf().path());
                    return this;
                })
                .onMessageEquals("create a child", () -> {
                    ActorRef<String> secondActor = getContext().spawn(FirstSimpleBehavior.create(), "secondActorSpawn_1");
                    secondActor.tell("who are u");
                    return this;
                })
                .onAnyMessage(msg -> {
                    System.out.println("Received msg : " + msg);
                    return this;
                })
                .build();
    }


}
