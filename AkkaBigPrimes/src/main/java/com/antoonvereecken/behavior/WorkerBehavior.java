package com.antoonvereecken.behavior;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Random;

public class WorkerBehavior extends AbstractBehavior<WorkerBehavior.Command> {
    /** CONSTRUCTOR + FACTORY */
    private WorkerBehavior(ActorContext<WorkerBehavior.Command> context) {
        super(context);
    }
    public static Behavior<WorkerBehavior.Command> create() {
        return Behaviors.setup(WorkerBehavior::new);
    }
    /** MSG/CMD */
    public static class Command implements Serializable {
        public static final long serialVersionUID = 1252327382416109935L;
        private final String message;
        private final ActorRef<ManagerBehavior.Command> from;

        public Command(String message, ActorRef<ManagerBehavior.Command> from) {
            this.message = message;
            this.from = from;
        }
        public String getMessage() { return message; }
        public ActorRef<ManagerBehavior.Command> getFrom() { return from; }
    }
    /** HANDLER */
    @Override
    public Receive<Command> createReceive() {
        return primeNeedsCalculationHandler();
    }
    public Receive<Command> primeNeedsCalculationHandler() {
        return newReceiveBuilder()
                    .onAnyMessage(command -> {
                        BigInteger result = new BigInteger(2000, new Random());
                        BigInteger prime = result.nextProbablePrime();
                        Random r = new Random();
                        if (r.nextInt(5) < 2)
                            command.getFrom().tell(new ManagerBehavior.ResultCommand(prime));
                        
                        return primeIsAlrdyCalculatedHandler(prime);
                    }).build();
    }
    public Receive<Command> primeIsAlrdyCalculatedHandler(BigInteger prime) {
        return newReceiveBuilder()
                .onAnyMessage(command -> {
                    Random r = new Random();
                    if (r.nextInt(5) < 2)
                        command.getFrom().tell(new ManagerBehavior.ResultCommand(prime));
                    return Behaviors.same();
                }).build();
    }

}