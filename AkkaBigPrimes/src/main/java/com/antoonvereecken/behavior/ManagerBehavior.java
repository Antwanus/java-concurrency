package com.antoonvereecken.behavior;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;

import java.io.Serializable;
import java.math.BigInteger;
import java.time.Duration;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;

public class ManagerBehavior extends AbstractBehavior<ManagerBehavior.Command> {
    /** CONSTRUCTOR + FACTORY */
    private ManagerBehavior(ActorContext<Command> context) {
        super(context);
    }
    public static Behavior<Command> create() {
        return Behaviors.setup(ManagerBehavior::new);
    }

    /** MSG/CMD */
    public interface Command extends Serializable { }
    public static class InstructionCommand implements Command {
        public static final long serialVersionUID = 8600609741636312660L;
        private final String message;
        private final ActorRef<SortedSet<BigInteger>> replyTo;

        public InstructionCommand(String message, ActorRef<SortedSet<BigInteger>> replyTo) {
            this.message = message;
            this.replyTo = replyTo;
        }
        public String getMessage() { return message; }
        public ActorRef<SortedSet<BigInteger>> getReplyTo() { return replyTo; }
		public static long getSerialversionuid() { return serialVersionUID;}
        
    }
    public static class ResultCommand implements Command {
        public static final long serialVersionUID = 9105170003252584123L;
        private final BigInteger prime;

        public ResultCommand(BigInteger prime) {
            this.prime = prime;
        }
        public BigInteger getPrime() { return prime; }
    }
    private static class NoResponseReceivedCommand implements Command {
        public static final long serialVersionUID = -5229012234989802614L;
        private final ActorRef<WorkerBehavior.Command> worker;

        public NoResponseReceivedCommand(ActorRef<WorkerBehavior.Command> worker) {
            this.worker = worker;
        }
        public ActorRef<WorkerBehavior.Command> getWorker() {
            return worker;
        }
    }

    /** HANDLER */
    private final SortedSet<BigInteger> primes = new TreeSet<>();
    private ActorRef<SortedSet<BigInteger>> replyTo;

    @Override
    public Receive<Command> createReceive() {
        return newReceiveBuilder()
            .onMessage(InstructionCommand.class, cmd -> {
                if (cmd.getMessage().equalsIgnoreCase("start")) {
                	this.replyTo = cmd.getReplyTo();
                    for (int i = 0; i < 20; i++) {
                        ActorRef<WorkerBehavior.Command> worker = getContext().spawn(
                                WorkerBehavior.create(),
                                "worker_" + UUID.randomUUID()
                        );
                        askWorkerForAPrime(worker);
                    }
                }
                return Behaviors.same();
            })
            .onMessage(ResultCommand.class, cmd -> {
                primes.add(cmd.getPrime());
                System.out.println("Received: " + primes.size() + " prime numbers");
                //TODO send the list of primes to the main method
                if (primes.size() == 20) {
                	this.replyTo.tell(primes);
                }
                return Behaviors.same();
            })
            .onMessage(NoResponseReceivedCommand.class, cmd -> {
                System.out.println("Retrying with worker : " + cmd.getWorker().path());
                askWorkerForAPrime(cmd.getWorker());
                return Behaviors.same();
            })
            .build();
    }

    private void askWorkerForAPrime (ActorRef<WorkerBehavior.Command> worker) {
        getContext().ask(
    /* responseClass: */ ManagerBehavior.Command.class,
           /* target: */ worker,
     /* responseTime: */ Duration.ofSeconds(5),
    /* createRequest: */ (me) -> new WorkerBehavior.Command("start", me),
  /* applyToResponse: */ (res, throwable) -> {
                            if (res != null) return res;
                            else {
                                System.out.println("Worker " + worker.path() + " failed to respond.");
                                return new NoResponseReceivedCommand(worker);
        }});
    }


}