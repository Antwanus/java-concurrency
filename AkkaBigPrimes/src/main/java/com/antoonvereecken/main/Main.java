package com.antoonvereecken.main;

import akka.actor.typed.ActorSystem;
import akka.actor.typed.javadsl.AskPattern;

import java.math.BigInteger;
import java.time.Duration;
import java.util.SortedSet;
import java.util.concurrent.CompletionStage;

import com.antoonvereecken.behavior.ManagerBehavior;

public class Main {
/** // FirstSimpleBehavior
    public static void main(String[] args) {
        ActorSystem<String> actorSystem = ActorSystem.create(FirstSimpleBehavior.create(), "firstActorSystem");

        // configured messages
        actorSystem.tell("say hi");                                         // hi
        actorSystem.tell("who are u");                                      // My path is akka://firstActorSystem/user
        actorSystem.tell("create a child");                                 // My path is akka://firstActorSystem/user/secondActorSpawn_1

        // non-configured messages
        actorSystem.tell("Hello, are you there God? It's me, Jesus...");    // Received msg : Hello, are you there God? It's me, Jesus...
        actorSystem.tell("Hello?");                                         // Received msg : Hello?
    }
*/
/** // BigPrimes exercise1: my solution
    public static void main(String[] args) {
        ActorSystem<MyWorkerBehavior.Command> mySystem = ActorSystem.create(MyWorkerBehavior.create(), "firstActorSystem");

        mySystem.tell(MyWorkerBehavior.CalculatePrime.INSTANCE);      // manager/firstActor returns 1 big prime

        // (exercise 1: my solution)
        for (int i = 0; i < 20; i++) {
            // firstActor spawns an actor, this actor gets a {Command.CalculatePrime.INSTANCE}-message
            mySystem.tell(MyWorkerBehavior.SpawnActor.INSTANCE);
        }
    }
*/

    public static void main(String[] args) {
       ActorSystem<ManagerBehavior.Command> bigPrimes = ActorSystem.create(ManagerBehavior.create(), "bigPrimes");
       bigPrimes.tell(new ManagerBehavior.InstructionCommand("start", null));
       
       CompletionStage<SortedSet<BigInteger>> result = AskPattern.ask(
    		   bigPrimes,
    		   (me) -> new ManagerBehavior.InstructionCommand("start", me), 
    		   Duration.ofSeconds(30),
    		   bigPrimes.scheduler()
	   );
       result.whenComplete((reply, failure) -> {
    	  if (reply != null) reply.forEach(System.out::println);
    	  else System.out.println("System did not respond in time");
    	  bigPrimes.terminate();
       });

    }

}