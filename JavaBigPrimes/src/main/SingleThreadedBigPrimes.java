package main;

import java.math.BigInteger;
import java.util.Random;
import java.util.SortedSet;
import java.util.TreeSet;

public class SingleThreadedBigPrimes {

    public static void main(String[] args) {
        long start = System.currentTimeMillis();

        SortedSet<BigInteger> primes = new TreeSet<>();
        while (primes.size() < 20) {
            BigInteger bigInteger = new BigInteger(2000, new Random());
            primes.add(bigInteger.nextProbablePrime());
        }


        long end = System.currentTimeMillis();
        System.out.println("Time elapsed: " + (end - start) + " ms");
        System.out.println(primes);


    }

}
