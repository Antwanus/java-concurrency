package main;

import java.math.BigInteger;
import java.util.SortedSet;
import java.util.TreeSet;

public class Results {
    /** BENCHMARK - GraalVM java11-20.3.0
     *      SingleThreadedBigPrimes:
     *      - Time elapsed: [10138, 8999, 10435, 14654, 16890, 15694] ms
     *
     *      MultiThreadedBigPrimes:
     *      - Time elapsed: [2235, 2605, 2144, 2176, 1896, 2634] ms
     *
     *      MultiThreadedBigPrimes (thread blocking)
     *      - Time elapsed: [6173, 7994, 6101, 7692, 6572, 9858] ms
     *
     * */
    private SortedSet<BigInteger> primes;

    public Results() {
        this.primes = new TreeSet<>();
    }

    public int getSize() {
        synchronized (this) {
            return primes.size();
        }
    }

    public void addPrime(BigInteger prime) {
        synchronized (this) {
            primes.add(prime);
        }
    }

    public void print() {
        synchronized (this) {
            primes.forEach(System.out::println);
        }
    }

}
