package com.antoonvereecken;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;

import java.io.Serializable;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

public class RaceController extends AbstractBehavior<RaceController.Command> {
    /** CONSTRUCTOR + FACTORY */
    private RaceController(ActorContext<Command> context) {
        super(context);
    }
    public static Behavior<Command> create() {
        return Behaviors.setup(RaceController::new);
    }
    /** MSG */
    public interface Command extends Serializable { }
    public static class StartRaceCommand implements Command {
        static final long serialVersionUID = 4271924683079409042L;
    }
    public static class UpdateRacerLocationCommand implements Command {
        static final long serialVersionUID = 4271924683079409042L;
        private final ActorRef<Racer.Command> racer;
        private final int position;

        public UpdateRacerLocationCommand(ActorRef<Racer.Command> racer, int position) {
            this.racer = racer;
            this.position = position;
        }
        public ActorRef<Racer.Command> getRacer() { return racer; }
        public int getPosition() { return position; }
    }
    // private because it's a msg from the controller to itself (like a schedule update)
    private static class RacerReturnPositionCommand implements Command {
        static final long serialVersionUID = 5675579268458856867L;
    }
    public static class RacerFinishedCommand implements Command {
        static final long serialVersionUID = -8069363614395885948L;
        private final ActorRef<Racer.Command> racer;

        public RacerFinishedCommand(ActorRef<Racer.Command> racer) {
            this.racer = racer;
        }
        public ActorRef<Racer.Command> getRacer() { return racer; }
    }



    /** HANDLER */
    private Map<ActorRef<Racer.Command>, Integer> racersCurrentPosition;
    private Map<ActorRef<Racer.Command>, Long> finishTimes;
    private long start;
    private static final int RACE_LENGTH = 100;
    private static final Object TIMER_KEY = null;

    @Override
    public Receive<Command> createReceive() {
        return newReceiveBuilder()
                .onMessage(StartRaceCommand.class, msg -> {
                    start = System.currentTimeMillis();
                    racersCurrentPosition = new HashMap<>();
                    finishTimes = new HashMap<>();
                    for (int i = 0; i < 10; i++) {
                        ActorRef<Racer.Command> racer = getContext().spawn(Racer.create(), "racer_" +i);
                        racersCurrentPosition.put(racer, 0);
                        racer.tell(new Racer.StartCommand(RACE_LENGTH));
                    }
                    return Behaviors.withTimers(timer -> {
                        timer.startTimerAtFixedRate(TIMER_KEY, new RacerReturnPositionCommand(), Duration.ofSeconds(1L));
                        return Behaviors.same();
                    });
                })
                .onMessage(RacerReturnPositionCommand.class, msg -> {
                    for (ActorRef<Racer.Command> racer : racersCurrentPosition.keySet()) {
                        racer.tell(new Racer.PositionCommand(getContext().getSelf()));
                        displayRace();
                    }
                    return Behaviors.same();
                })
                .onMessage(UpdateRacerLocationCommand.class, msg -> {
                    racersCurrentPosition.put(msg.racer, msg.position);
                    return Behaviors.same();
                })
                .onMessage(RacerFinishedCommand.class, msg -> {
                    finishTimes.put(msg.racer, System.currentTimeMillis());
                    if (finishTimes.size() == 10) {
                        return raceCompleteHandler();
                    } else return Behaviors.same();
                })
                .build();
    }

    public Receive<Command> raceCompleteHandler() {
        /** shut down all of the children (spawns) & shut down the ActorSystem (first actor) */
        return newReceiveBuilder()
                .onMessage(RacerReturnPositionCommand.class, msg -> {
                    for (ActorRef<Racer.Command> racer : racersCurrentPosition.keySet()) {
                        getContext().stop(racer);               // stop spawn
                    }
                    displayResults(finishTimes);
                    return Behaviors.withTimers(timers -> {
                        timers.cancelAll();                     // stop all that inherit from system
                        return Behaviors.stopped();
                    });
                })
                .build();
    }

    private void displayRace() {
        int displayLength = 160;
        for (int i = 0; i < 50; ++i) System.out.println();
        System.out.println("Race has been running for " + ((System.currentTimeMillis() - start) / 1000) + " seconds.");
        System.out.println("    " + new String (new char[displayLength]).replace('\0', '='));
        int i = 0;
        for (ActorRef<Racer.Command> racer : racersCurrentPosition.keySet()) {
            System.out.println(i + " : "  + new String (new char[racersCurrentPosition.get(racer) * 160 / 100]).replace('\0', '*'));
            i++;
        }
    }
    private void displayResults(Map<ActorRef<Racer.Command>, Long> results) {
        System.out.println("Results");
        results.values().stream()
                .sorted().forEach(result -> {
                    for(ActorRef<Racer.Command> racer : results.keySet()) {
                        if ( results.get(racer) == result ) {
                            String racerId = racer.path().toString()
                                    .substring(
                                        racer.path().toString().length() -1
                            );
                            System.out.println("Racer " + racerId + " finished in " + ((double)result - start) / 1000 + "s");
        }}});
    }


}
