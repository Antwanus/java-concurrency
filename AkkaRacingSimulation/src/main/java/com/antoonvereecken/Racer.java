package com.antoonvereecken;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.PostStop;
import akka.actor.typed.javadsl.*;

import java.io.Serializable;
import java.util.Random;

public class Racer extends AbstractBehavior<Racer.Command> {
    /** CONSTRUCTOR & FACTORY */
    private Racer(ActorContext<Command> context) {
        super(context);
    }
    public static Behavior<Command> create() {
        return Behaviors.setup(Racer::new);
    }

    /** MSG/CMD */
    public interface Command extends Serializable { }

    public static class StartCommand implements Command {
        static final long serialVersionUID = -5009176246969983597L;
        private final int raceLength;

        public StartCommand(int raceLength) {
            this.raceLength = raceLength;
        }
        public int getRaceLength() { return raceLength; }
    }
    public static class PositionCommand implements Command {
        static final long serialVersionUID = -5440978430250424920L;
        private final ActorRef<RaceController.Command> replyTo;

        public PositionCommand(ActorRef<RaceController.Command> replyTo) {
            this.replyTo = replyTo;
        }
        public ActorRef<RaceController.Command> getReplyTo() { return replyTo; }
    }

    /** HANDLER */
    private static final double DEFAULT_AVERAGE_SPEED = 48.2;
    private int averageSpeedAdjustmentFactor;
    private Random random;
    private double currentSpeed = 0;

    @Override
    public Receive<Command> createReceive() {
        return raceNotStartedHandler();
    }
    public Receive<Command> raceNotStartedHandler() {
        return newReceiveBuilder()
                .onMessage(StartCommand.class, msg -> {
                    this.random = new Random();
                    this.averageSpeedAdjustmentFactor = random.nextInt(30) - 10;
                    return raceInProgressHandler(msg.raceLength, 0);
                })
                .onMessage(PositionCommand.class, msg -> {
                    msg.getReplyTo().tell(new RaceController.UpdateRacerLocationCommand(
                            getContext().getSelf(),
                            0
                    ));
                    return Behaviors.same();
                })
                .build();
    }
    public Receive<Command> raceInProgressHandler(int raceLength, int currentPosition) {
        return newReceiveBuilder()
                .onMessage(Racer.PositionCommand.class, msg -> {
                    determineNextSpeed(currentPosition, raceLength);
                    int newPosition = currentPosition;
                    newPosition += getDistanceMovedPerSecond();
                    if (newPosition > raceLength ) newPosition  = raceLength;
                    msg.getReplyTo().tell(
                            new RaceController.UpdateRacerLocationCommand(
                                    getContext().getSelf(),
                                    newPosition
                    ));
                    if (newPosition == raceLength) return racerFinishedHandler(raceLength);
                    else return raceInProgressHandler(raceLength, newPosition);
                })
                .build();
    }
    public Receive<Command> racerFinishedHandler(int raceLength) {
        return newReceiveBuilder()
                .onMessage(PositionCommand.class, msg -> {
                    msg.getReplyTo().tell(new RaceController.UpdateRacerLocationCommand(
                            getContext().getSelf(),
                            raceLength
                    ));
                    msg.getReplyTo().tell(new RaceController.RacerFinishedCommand(
                            getContext().getSelf()
                    ));
//                    return Behaviors.stopped();       // returns a log.warn everytime it gets called
//                    return Behaviors.ignore();        // does not return a log.warn
                    return waitingToStop();             // returns a behavior that is still open for signals
                })
                .build();
    }
    public Receive<Command> waitingToStop() {
        return newReceiveBuilder()
                .onAnyMessage(msg -> {
                    return Behaviors.same();            // still allow msg to be sent (no warns or error logs)
                })
                .onSignal(PostStop.class, postStopSignal -> {
                    /** close resources here */
                    // Slf4j <3
                    if (getContext().getLog().isInfoEnabled()) {
                        getContext().getLog().info("Termination imminent");
                    }
                    return Behaviors.same();
                })
                .build();
    }

    /** HELPER */
    private double getMaxSpeed() {
        return DEFAULT_AVERAGE_SPEED * (1+ ((double)averageSpeedAdjustmentFactor / 100) );
    }
    private double getDistanceMovedPerSecond() {
        return currentSpeed * 1000 / 3600;
    }
    private void determineNextSpeed(int currentPosition, int raceLength) {
        if (currentPosition < (raceLength / 4))
            currentSpeed = currentSpeed  + (((getMaxSpeed() - currentSpeed) / 10) * random.nextDouble());
        else currentSpeed = currentSpeed * (0.5 + random.nextDouble());
        if (currentSpeed > getMaxSpeed()) currentSpeed = getMaxSpeed();
        if (currentSpeed < 5) currentSpeed = 5;
        if (currentPosition > (raceLength / 2) && currentSpeed < getMaxSpeed() / 2)
            currentSpeed = getMaxSpeed() / 2;

    }
}
