package blockchain;

import akka.actor.typed.ActorRef;
import akka.actor.typed.Behavior;
import akka.actor.typed.javadsl.AbstractBehavior;
import akka.actor.typed.javadsl.ActorContext;
import akka.actor.typed.javadsl.Behaviors;
import akka.actor.typed.javadsl.Receive;
import model.Block;
import model.HashResult;

import java.io.Serializable;
import java.util.Objects;

public class ManagerBehavior extends AbstractBehavior<ManagerBehavior.Command> {
    //Command
    public interface Command extends Serializable {}

    public static class MineBlockCommand implements Command {
        static final long serialVersionUID = 8438558968097243469L;
        private Block block;
        private ActorRef<HashResult> sender;
        private int difficulty;

        public MineBlockCommand(Block block, ActorRef<HashResult> sender, int difficulty) {
            this.block = block;
            this.sender = sender;
            this.difficulty = difficulty;
        }
        public Block getBlock() { return block; }
        public ActorRef<HashResult> getSender() { return sender; }
        public int getDifficulty() { return difficulty; }
    }

    public static class HashResultCommand implements Command {
        static final long serialVersionUID = -3827979988014935574L;
        private HashResult hashResult;

        public HashResultCommand(HashResult hashResult) {
            this.hashResult = hashResult;
        }
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            HashResultCommand that = (HashResultCommand) o;
            return Objects.equals(hashResult, that.hashResult);
        }
        @Override
        public int hashCode() {
            return Objects.hash(hashResult);
        }
        public HashResult getHashResult() { return hashResult; }
    }

    //Construct
    private ManagerBehavior(ActorContext<Command> context) {
        super(context);
    }
    public static Behavior<Command> create() {
        return Behaviors.setup(ManagerBehavior::new);
    }

    //Handle
    private ActorRef<HashResult> sender;
    private Block block;
    private int difficulty;
    private int currentNonce = 0;

    @Override
    public Receive<Command> createReceive() {
        return newReceiveBuilder().onMessage(MineBlockCommand.class,
                cmd -> {
                    this.sender = cmd.getSender();
                    this.block = cmd.getBlock();
                    this.difficulty = cmd.getDifficulty();
                    for (int i = 0; i < 10; i++) {
                        startNextWorker();
                    }
                    return Behaviors.same();
                })
                .build();
    }
    // spawn worker & give next task
    private void startNextWorker() {
        ActorRef<WorkerBehavior.Command> worker = getContext().spawn(
                WorkerBehavior.create(), ("worker_" + currentNonce)
        );
        worker.tell(new WorkerBehavior.Command(
                block,
                currentNonce * 1000,
                difficulty,
                getContext().getSelf())
        );
        currentNonce++;

    }
}
