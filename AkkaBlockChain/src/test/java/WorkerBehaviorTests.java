import akka.actor.testkit.typed.CapturedLogEvent;
import akka.actor.testkit.typed.javadsl.BehaviorTestKit;
import akka.actor.testkit.typed.javadsl.TestInbox;
import blockchain.ManagerBehavior;
import blockchain.WorkerBehavior;
import model.Block;
import model.HashResult;
import org.junit.jupiter.api.Test;
import org.slf4j.event.Level;
import utils.BlocksData;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class WorkerBehaviorTests {
    @Test   // WorkerBehavior.createReceive() (FAIL)
    void testMiningFailsIfNonceNotInRange() {
        BehaviorTestKit<WorkerBehavior.Command> testActor = BehaviorTestKit.create(WorkerBehavior.create());
        Block block = BlocksData.getNextBlock(0, "0");
        TestInbox<ManagerBehavior.Command> controllerActorStub = TestInbox.create();
        WorkerBehavior.Command msg = new WorkerBehavior.Command(
                block,
                0,
                5,
                controllerActorStub.getRef()
        );

        testActor.run(msg);
        List<CapturedLogEvent> logEntries = testActor.getAllLogEntries();

        assertEquals(1, logEntries.size(), "testActor.getAllLogEntries().getSize() invalid");
        assertEquals(Level.DEBUG, logEntries.get(0).level(), "logging level is invalid");
        assertEquals("null", logEntries.get(0).message(), "the message invalid: must be \"null\" (-> none found) ");
    }

    @Test   // WorkerBehavior.createReceive() (SUCCESS)
    void testMiningSuccessIfNonceIsInRange() {
        BehaviorTestKit<WorkerBehavior.Command> testActor = BehaviorTestKit.create(WorkerBehavior.create());
        Block block = BlocksData.getNextBlock(0, "0");
        TestInbox<ManagerBehavior.Command> controllerActorStub = TestInbox.create();
        WorkerBehavior.Command msg = new WorkerBehavior.Command(
                block,
                930001,
                5,
                controllerActorStub.getRef()
        );
        String expectedResult = "930724 : 00000094ca51e739f86a02e745de69d9943fc7c1c06b629d3f605aa077f71e74";

        testActor.run(msg);
        List<CapturedLogEvent> logEntries = testActor.getAllLogEntries();

        assertEquals(1, logEntries.size(), "testActor.getAllLogEntries().getSize() invalid");
        assertEquals(Level.DEBUG, logEntries.get(0).level(), "logging level invalid: must be \"DEBUG\"");
        assertEquals(expectedResult, logEntries.get(0).message(), "mining result invalid: \"NONCE: HASH\"");
    }

    @Test
    void testMiningSuccessSendsMsgToController() {
        BehaviorTestKit<WorkerBehavior.Command> testActor = BehaviorTestKit.create(WorkerBehavior.create());
        Block block = BlocksData.getNextBlock(0, "0");
        TestInbox<ManagerBehavior.Command> controllerActorStub = TestInbox.create();

        WorkerBehavior.Command msg = new WorkerBehavior.Command(
                block,
                930001,
                5,
                controllerActorStub.getRef()
        );
        testActor.run(msg);
        HashResult expectedHashResult = new HashResult();
        expectedHashResult.foundAHash(
                "00000094ca51e739f86a02e745de69d9943fc7c1c06b629d3f605aa077f71e74",
                930724
        );
        ManagerBehavior.Command expectedCommand = new ManagerBehavior.HashResultCommand(expectedHashResult);
        controllerActorStub.expectMessage(expectedCommand);

    }

    @Test
    void testMiningFailDoesNotSendMsgToController() {
        BehaviorTestKit<WorkerBehavior.Command> testActor = BehaviorTestKit.create(WorkerBehavior.create());
        Block block = BlocksData.getNextBlock(0, "0");
        TestInbox<ManagerBehavior.Command> controllerActorStub = TestInbox.create();

        WorkerBehavior.Command msg = new WorkerBehavior.Command(
                block,
                0,
                5,
                controllerActorStub.getRef()
        );
        testActor.run(msg);

        assertFalse(controllerActorStub.hasMessages(), "controllerActorStub should not have received any msg");
    }


}
